class TypingText {
  constructor(element, options = {}) {
    this.element = document.querySelector(element);
    this.type = 'print';                                                            // delete or print
    this.words = options.words || TypingText.getDataWords(this.element);
    this.index = options.index || 0;
    this.currentIndex = this.index;
    this.currentWord = this.words[this.currentIndex];
    this.speedPrint = options.speedPrint || 200;
    this.speedDelete = options.speedDelete || 150;
    this.speed = options.speed || 200;
    this.delay = options.delay || 2000;

    document.addEventListener('DOMContentLoaded', () => {
      this.init();
    });
  }

  init() {
    this.currentIndex = this.index % this.words.length;

    if (this.type === 'delete') {
      this.currentWord = this.words[this.currentIndex].substring(0, this.currentWord.length - 1);
      this.speed = this.speedDelete;
    }

    if (this.type === 'print') {
      this.currentWord = this.words[this.currentIndex].substring(0, this.currentWord.length + 1);
      this.speed = this.speedPrint;
    }

    if (this.type === 'print' && this.currentWord === this.words[this.currentIndex]) {
      this.type = 'delete';
      this.speed = this.delay;
    }

    if (this.type === 'delete' && this.currentWord === '') {
      this.type = 'print';
      this.speed = this.speedDelete + this.speedPrint;
      this.index++;
    }

    this.element.textContent = this.currentWord;

    setTimeout(() => this.init(), this.speed);
  }

  static getDataWords(elem) {
    let arr = elem.dataset.words.split('\'');

    return arr.filter((el, i) => i % 2 !== 0);
  }
}

export default TypingText;